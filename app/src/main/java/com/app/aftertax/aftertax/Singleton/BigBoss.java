package com.app.aftertax.aftertax.Singleton;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.app.aftertax.aftertax.MainActivity;
import com.app.aftertax.aftertax.Model.Category;
import com.app.aftertax.aftertax.Model.Country;
import com.app.aftertax.aftertax.Model.JSONSource;
import com.app.aftertax.aftertax.Model.MainTax;
import com.app.aftertax.aftertax.Model.State;
import com.app.aftertax.aftertax.Model.Tax;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by suhailbhat on 27/08/15.
 */
public class BigBoss extends Service {
    private static final String TAG = BigBoss.class.getSimpleName();
    private static BigBoss ourInstance = new BigBoss();
    public State[] states;
    public Category[] categories;
    public ArrayList<String> categoriesToShow;
    public Country[] countries;
    public ArrayList<State> currentStates;
    public Country currentCountrySelected;
    public State currentStateSelected;
    public Category currentCategorySelected;
    public float currentTippingPercentage;
    public MainTax[] mainTaxes;
    public Tax[] taxes;
    public Activity mainActivity;
    public Map<Integer, String> categoryMap = new HashMap<Integer, String>();
    ProgressDialog mProgressDialog;
    private ParseFile taxRatesJsonParseFile;
    private byte[] taxRatesJson;

    private BigBoss() {

    }

    public static BigBoss getInstance() {
        return ourInstance;
    }

    // Send an Intent with an action named "custom-event-name". The Intent
    // sent should
    // be received by the ReceiverActivity.
    private static void sendMessage() {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent("categories_received");
        // You can also include some extra data.
        intent.putExtra("message", "This is my message!");
        ourInstance.mProgressDialog.dismiss();
        LocalBroadcastManager.getInstance(ourInstance).sendBroadcast(intent);
    }

    public static JsonObject convertFileToJSON(String fileName) {
        // Read from File to String
        JsonObject jsonObject = new JsonObject();

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(fileName));
            jsonObject = jsonElement.getAsJsonObject();
        } catch (FileNotFoundException e) {

        } catch (IOException ioe) {

        }
        return jsonObject;
    }

    public static boolean isInternetAvailable(Context context) {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null) {
            Log.d(TAG, "no internet connection");
            return false;
        } else {
            if (info.isConnected()) {
                Log.d(TAG, " internet connection available...");
                return true;
            } else {
                Log.d(TAG, " internet connection");
                return true;
            }

        }
    }

    public void initialize() {
        // Do multiple operations here like fetch data from server..
        // Fetch Countries
        //this.fetchCountries();
        //this.fetchCategories();
    }

    public void dInitialize(Activity main) {
        // Do multiple operations here like fetch data from server..
        this.mainActivity = main;
        // Fetch Data
        this.fetchData();
    }

    public void setCurrentCategorySelected(String categoryName) {
        this.currentCategorySelected = this.getCategoryByName(categoryName);
    }

    public void setCurrentStateSelected(State state) {
        //Generate taxes
        this.currentStateSelected = state;
        // Set persistently .
        this.mainTaxes = this.currentStateSelected.taxes;
        for (int i = 0; i < this.mainTaxes.length; i++) {
            if (this.mainTaxes[i].id.equals("1")) {  // Sales Tax
                this.taxes = this.mainTaxes[i].taxes;
                break;
            }
        }
    }

    private void fetchData() {
        // Create a progressdialog
//        if (!isInternetAvailable(ourInstance.mainActivity)) {
//            return;
//        }
        //currentCountrySelected = "India";
        mProgressDialog = new ProgressDialog(ourInstance.mainActivity);
        mProgressDialog.setCancelable(true);
        // Set progressdialog title
        mProgressDialog.setTitle("Please wait");
        // Set progressdialog message
        mProgressDialog.setMessage("Fetching latest data... ");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setProgress(0);
        // Show progressdialog
        mProgressDialog.show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("TaxJson");
        query.setLimit(1);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(final List<ParseObject> json, ParseException e) {
                if (e == null) {
                    ParseObject object = json.get(0);
                    ParseFile testFile = object.getParseFile("JSON");
                    testFile.getDataInBackground(new GetDataCallback() {

                        public void done(byte[] data, ParseException e) {
                            if (e == null) {
                                // data has the bytes for the resume
                                String jsonString = new String(data);
                                Gson gson = new Gson();
                                JSONSource jsonSource = gson.fromJson(jsonString, JSONSource.class);
                                countries = jsonSource.countries;
                                states = jsonSource.states;
                                categories = jsonSource.categories;
                                ourInstance.generateCategoriesToShow();
                                ourInstance.sendMessage();
                                //MainActivity.setupActionBarWithElements(categoriesToShow);
                            } else {
                                // something went wrong
                                Log.e("", e.getMessage());
                            }
                        }
                    }, new ProgressCallback() {
                        @Override
                        public void done(Integer integer) {
                            mProgressDialog.setProgress(integer);
                            if (integer == 100)
                                mProgressDialog.dismiss();
                        }
                    });
                } else {
                    mProgressDialog.dismiss();
                    Log.d("Categories", "Error: " + e.getMessage());
                }
            }
        });
    }

    public String[] getCountryNames() {
        if (BigBoss.getInstance().countries == null) return null;
        String[] countryNames = new String[BigBoss.getInstance().countries.length];
        for (int i = 0; i < BigBoss.getInstance().countries.length; i++) {
            countryNames[i] = BigBoss.getInstance().countries[i].name;
        }
        return countryNames;
    }

    // This will return state names for the country passed.
    public ArrayList<State> getStateForCountry(String countryName) {
        currentStates = new ArrayList<State>();
        for (State eachState : ourInstance.states) {
            if (eachState.country.equals(countryName)) {
                // States with country as countryName as passed above
                currentStates.add(eachState);
            }
        }
        return currentStates;
    }

    public String[] getStateNamesForCountry(String countryName) {
        ourInstance.getStateForCountry(countryName);
        ArrayList<String> stateNames = new ArrayList<String>();
        for (State eachState : ourInstance.currentStates) {
            stateNames.add(eachState.name);
        }
        return stateNames.toArray(new String[stateNames.size()]);
    }

    public ArrayList<String> generateCategoriesToShow() {
        categoriesToShow = new ArrayList<String>();
        for (Category eachCategory : categories) {
            if (eachCategory.taxesIDs == null || eachCategory.indexToPut == -1) break;
            Log.d("Category parse", eachCategory.name);
            categoriesToShow.add(eachCategory.name);
        }
        return categoriesToShow;
    }

    public Category getCategoryByName(String categoryName) {
        for (Category eachCategory : categories) {
            if (eachCategory.name == categoryName)
                return eachCategory;
        }
        return null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        sendMessage();
        return super.onStartCommand(intent, flags, startId);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(MainActivity.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public enum CategoryEnums {
        cRestaurantDinner(1),
        cGroceries(2),
        cClothes(3),
        cConsumerProducts(4),
        cDiesel(5),
        cGasoline(6),
        cCigarettes(7),
        cPrescribedDrugs(8),
        cNonPrescribedDrugs(9),
        cAlcohol(10),
        cElectronics(11),
        cJewellery(12),
        cService(13),
        cLiquor(14),
        cBeer(15),
        cWine(16),
        cChampangne(17);


        private int statusId;

        CategoryEnums(int statusId) {
            this.statusId = statusId;
        }

        public static CategoryEnums getById(int statusId) {
            switch (statusId) {
                case 1:
                    return cRestaurantDinner;
                case 2:
                    return cGroceries;
                case 3:
                    return cClothes;
                case 4:
                    return cConsumerProducts;
                case 5:
                    return cDiesel;
                case 6:
                    return cGasoline;
                case 7:
                    return cCigarettes;
                case 8:
                    return cPrescribedDrugs;
                case 9:
                    return cNonPrescribedDrugs;
                case 10:
                    return cAlcohol;
                case 11:
                    return cElectronics;
                case 12:
                    return cJewellery;
                case 13:
                    return cService;
                case 14:
                    return cLiquor;
                case 15:
                    return cBeer;
                case 16:
                    return cWine;
                case 17:
                    return cChampangne;
                default:
                    return null;
            }
        }

        public int getStatusId() {
            return statusId;
        }
    }


    public enum TaxEnumerator {
        GeneralTax(1),
        TotalMaximumSurtax(2),
        Gasoline(3),
        Cigarettes(4),
        Groceries(5),
        RestaurantDinner(6),
        PrescribedDrugs(7),
        NonPrescribedDrugs(8),
        Clothes(9),
        Diesel(10),
        AverageLocalSurtax(11),
        MaximumLocalSurtax(12),
        Beer(13),
        Wine(14),
        SparklingHard(15),
        Champagne(16),
        Jewellery(17),
        Liquor(18);


        private int statusId;

        TaxEnumerator(int statusId) {
            this.statusId = statusId;
        }

        public static TaxEnumerator getById(int statusId) {
            switch (statusId) {
                case 1:
                    return GeneralTax;
                case 2:
                    return TotalMaximumSurtax;
                case 3:
                    return Gasoline;
                case 4:
                    return Cigarettes;
                case 5:
                    return Groceries;
                case 6:
                    return RestaurantDinner;
                case 7:
                    return PrescribedDrugs;
                case 8:
                    return NonPrescribedDrugs;
                case 9:
                    return Clothes;
                case 10:
                    return Diesel;
                case 11:
                    return AverageLocalSurtax;
                case 12:
                    return MaximumLocalSurtax;
                case 13:
                    return Beer;
                case 14:
                    return Wine;
                case 15:
                    return SparklingHard;
                case 16:
                    return Champagne;
                case 17:
                    return Jewellery;
                case 18:
                    return Liquor;
                default:
                    return GeneralTax;
            }
        }

        public int getStatusId() {
            return statusId;
        }
    }
}
