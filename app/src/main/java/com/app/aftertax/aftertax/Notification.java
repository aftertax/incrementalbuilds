package com.app.aftertax.aftertax;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hanan on 14/09/15.
 */
public class Notification extends AppCompatActivity {
    ListView notifview;
    String[] notifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);

        // Get ListView object from xml
        notifview = (ListView) findViewById(R.id.notificationid); // When you are trying to find a View Noooo itth isshould be in

        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("username", ParseUser.getCurrentUser().getUsername());


        try {
            Object o = ParseCloud.callFunction("fetchNotifications", params);
            ArrayList<ParseObject> arrayList = (ArrayList<ParseObject>) o;
            String title = null;
            String[] feeds = new String[((ArrayList<ParseObject>) o).size()];
            for (int i = 0; i < arrayList.size(); i++) {
                System.out.println(arrayList.get(i));
                ParseObject parseObject = (ParseObject) arrayList.get(i);
                title = "";
                title = (String) parseObject.get("title");
                feeds[i] = title;
            }
            notifications = new String[feeds.length];
            notifications = feeds;
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, notifications);
            // Assign adapter to ListView
            notifview.setAdapter(adapter); // It will crash here !Yes ysdapter(adapter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // ListView Item Click Listener
        notifview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) notifview.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                        .show();

            }

        });
    }
}
