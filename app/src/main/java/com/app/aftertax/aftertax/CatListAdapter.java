package com.app.aftertax.aftertax;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by hanan on 27/08/15.
 */
public class CatListAdapter extends BaseAdapter {
    ArrayList<String> data;
    LayoutInflater mInflator;
    private Context ctx;

    public CatListAdapter(Context ctx, ArrayList<String> data) {
        this.ctx = ctx;
        this.data = data;
        mInflator = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = mInflator.inflate(R.layout.catlist, parent, false);
        TextView txtComp = (TextView) row.findViewById(R.id.categorytextview);
        txtComp.setText(data.get(position));

        ImageView iconComp = (ImageView) row.findViewById(R.id.image);
        iconComp.setImageResource(getResourceIdByName(data.get(position)));

        return row;
    }


    private int getResourceIdByName(String name) {
        // ResourceName = lowercase category name with removed white spaces.
        String resourceName = name.toLowerCase();  // Makes Restaurant Dinner to restaurant dinner.
        resourceName = resourceName.replace(" ", "");  // Makes restaurant dinner to restaurantdinner (which is the name of image file)!
        int drawableResourceId = ctx.getResources().getIdentifier(resourceName, "drawable", ctx.getPackageName()); // gets resource ID from its name.
        return drawableResourceId; // returns the resource id. C
    }
}
