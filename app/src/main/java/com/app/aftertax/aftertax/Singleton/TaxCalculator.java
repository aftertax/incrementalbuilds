package com.app.aftertax.aftertax.Singleton;

import com.app.aftertax.aftertax.Model.Tax;

import java.util.ArrayList;

/**
 * Created by suhailbhat on 27/08/15.
 */

public class TaxCalculator {

    private static TaxCalculator ourInstance = new TaxCalculator();
    public Boolean isMinTax = false;

    private TaxCalculator() {
    }

    public static TaxCalculator getInstance() {
        return ourInstance;
    }

    public float getAfterTaxForAmountAndCategory(float beforeTax) {

        // This method will take amount which is passed and calculate tax on that and return the AT
        // This class or any Non Activity class need not to do anything on UI!

        ArrayList<Tax> taxesToApply = new ArrayList<Tax>();
        float afterTax = this.calculateTippedAmount(beforeTax);
        float genTaxPercentage = 0.0f;
        Tax rateTax = new Tax();

        if (BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id) == BigBoss.CategoryEnums.cChampangne
                || BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id) == BigBoss.CategoryEnums.cAlcohol
                || BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id) == BigBoss.CategoryEnums.cBeer
                || BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id) == BigBoss.CategoryEnums.cLiquor
                || BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id) == BigBoss.CategoryEnums.cWine
                || BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id) == BigBoss.CategoryEnums.cCigarettes) {
            // Rate Calculation in these cases.
            switch (BigBoss.CategoryEnums.getById(BigBoss.getInstance().currentCategorySelected.id)) {
                case cCigarettes:
                    rateTax = BigBoss.getInstance().taxes[BigBoss.TaxEnumerator.Cigarettes.getStatusId()];
                    break;
                case cAlcohol:
                    rateTax = BigBoss.getInstance().taxes[BigBoss.TaxEnumerator.Liquor.getStatusId()];
                    break;
                case cLiquor:
                    rateTax = BigBoss.getInstance().taxes[BigBoss.TaxEnumerator.Liquor.getStatusId()];
                    break;
                case cBeer:
                    rateTax = BigBoss.getInstance().taxes[BigBoss.TaxEnumerator.Beer.getStatusId()];
                    break;
                case cWine:
                    rateTax = BigBoss.getInstance().taxes[BigBoss.TaxEnumerator.Wine.getStatusId()];
                    break;
                case cChampangne:
                    rateTax = BigBoss.getInstance().taxes[BigBoss.TaxEnumerator.Champagne.getStatusId()];
                    break;
                default:
                    break;
            }
            if (rateTax.percent == 0.0f) {
                float rateAT = beforeTax + rateTax.rate;
                return rateAT;
            }
        }

        for (int i = 0; i < BigBoss.getInstance().taxes.length; i++) {
            Tax tax = BigBoss.getInstance().taxes[i];
            String taxToConsider;
            if (this.isMinTax) {
                taxToConsider = "General Tax";
            } else {
                taxToConsider = "Total Max Surtax";
            }
            if (tax.name.equals(taxToConsider)) {
                genTaxPercentage = tax.percent;
            }
            for (int j = 0; j < BigBoss.getInstance().currentCategorySelected.taxesIDs.length; j++) {
                if (BigBoss.getInstance().currentCategorySelected.taxesIDs[j] == tax.id) {
                    taxesToApply.add(tax);
                }
            }
        }
        float collectiveTaxPercentage = 0.0f;

        for (int i = 0; i < taxesToApply.size(); i++) {
            Tax tax = taxesToApply.get(i);
            if (tax.GeneralTax == true) {
                collectiveTaxPercentage = genTaxPercentage;
                break;
            }
            if (tax.unit != null) {
                //do something
            }
            collectiveTaxPercentage += tax.percent;
        }

        float toPayAmount = (beforeTax * collectiveTaxPercentage) / 100 + beforeTax;
        return toPayAmount;
    }

    public float calculateTippedAmount(float beforeTip) {
        return ((beforeTip * BigBoss.getInstance().currentTippingPercentage) / 100) + beforeTip;
    }
}