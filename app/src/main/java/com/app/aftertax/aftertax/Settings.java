package com.app.aftertax.aftertax;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.app.aftertax.aftertax.Model.Country;
import com.app.aftertax.aftertax.Model.State;
import com.app.aftertax.aftertax.Singleton.BigBoss;
import com.parse.ParseUser;

public class Settings extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, NumberPicker.OnValueChangeListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Country Picker
        final NumberPicker array1 = (NumberPicker) findViewById(R.id.countrypicker);
        array1.setMinValue(0);
        array1.setMaxValue(BigBoss.getInstance().countries.length - 1);
        array1.setDisplayedValues(BigBoss.getInstance().getCountryNames());
        array1.setWrapSelectorWheel(true);
        array1.setOnValueChangedListener(this);

        //statepicker
        NumberPicker statepick = (NumberPicker) findViewById(R.id.statepicker);
        statepick.setWrapSelectorWheel(true);
        statepick.setOnValueChangedListener(this);

        //Tip picker
        NumberPicker tippick = (NumberPicker) findViewById(R.id.tippicker);
        tippick.setDisplayedValues(new String[]{"5%", "10%", "15%", "20%", "25%"});
        tippick.setMinValue(1);// restricted number to minimum value i.e 1
        tippick.setMaxValue(5);// restricked number to maximum value i.e. 31
        tippick.setWrapSelectorWheel(true);
        tippick.setOnValueChangedListener(this);

    }

    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        if (picker.getId() == R.id.countrypicker) //
        {
            Country selectedCountry = BigBoss.getInstance().countries[newVal];
            BigBoss.getInstance().currentCountrySelected = selectedCountry;
            TextView tvcountry = (TextView) findViewById(R.id.countryval);
            tvcountry.setText(selectedCountry.name);
        }
        if (picker.getId() == R.id.statepicker) {
            State selectedState = BigBoss.getInstance().getStateForCountry(BigBoss.getInstance().currentCountrySelected.name).get(newVal);
            BigBoss.getInstance().setCurrentStateSelected(selectedState);
            TextView tvstate = (TextView) findViewById(R.id.stateval);
            tvstate.setText(selectedState.name);
        }
    }

    public void countryClicked(View v) {
        NumberPicker np = (NumberPicker) findViewById(R.id.countrypicker);
        NumberPicker statepick = (NumberPicker) findViewById(R.id.statepicker);
        NumberPicker tippick = (NumberPicker) findViewById(R.id.tippicker);
        TextView countrytv = (TextView) findViewById(R.id.countryval);
        TextView statetv = (TextView) findViewById(R.id.stateval);
        TextView tiptv = (TextView) findViewById(R.id.tipval);
        statepick.setVisibility((View.GONE));
        tippick.setVisibility((View.GONE));
        if (np.getVisibility() == View.VISIBLE) {
            np.setVisibility(View.GONE);
        } else {
            np.setVisibility(View.VISIBLE);
        }

    }


    public void stateClicked(View v) {

        NumberPicker np = (NumberPicker) findViewById(R.id.countrypicker);

        NumberPicker statepick = (NumberPicker) findViewById(R.id.statepicker);

        statepick.setMinValue(0);
        String name = "India";
        if (BigBoss.getInstance().currentCountrySelected != null) {
            name = BigBoss.getInstance().currentCountrySelected.name;
        }
        statepick.setMaxValue(BigBoss.getInstance().getStateNamesForCountry(name).length - 1);
        statepick.setDisplayedValues(BigBoss.getInstance().getStateNamesForCountry(name));  // Get selected Country Above

        NumberPicker tippick = (NumberPicker) findViewById(R.id.tippicker);
        np.setVisibility((View.GONE));
        tippick.setVisibility((View.GONE));

        if (statepick.getVisibility() == View.VISIBLE) {
            statepick.setVisibility(View.GONE);

        } else {
            statepick.setVisibility(View.VISIBLE);
        }

    }

    public void tipClicked(View v) {
        NumberPicker np = (NumberPicker) findViewById(R.id.countrypicker);

        NumberPicker statepick = (NumberPicker) findViewById(R.id.statepicker);

        NumberPicker tippick = (NumberPicker) findViewById(R.id.tippicker);
        np.setVisibility(View.GONE);
        statepick.setVisibility((View.GONE));
        if (tippick.getVisibility() == View.VISIBLE) {

            tippick.setVisibility((View.GONE));
        } else {
            tippick.setVisibility(View.VISIBLE);
        }

    }


    public void aboutUsclicked(View v) {

        Intent aboutus1 = new Intent(v.getContext(), AboutUs.class);
        startActivity(aboutus1);

    }


    public void feedBackclick(View v) {

        Intent feed1 = new Intent(v.getContext(), FeedBack.class);
        startActivity(feed1);

    }

    public void logout(View v) {
        TextView logout = (TextView) findViewById(R.id.logout);

        // Logout Button Click Listener
        logout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Logout current userParseUser currentUser = ParseUser.getCurrentUser();

                ParseUser.logOut();

                ParseUser currentUser = ParseUser.getCurrentUser();
                currentUser.setEmail("");
                currentUser.setPassword("");
                currentUser.setUsername("");

                Intent homebk = new Intent(v.getContext(), MainActivity.class);
                startActivity(homebk);


            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

    }
}
