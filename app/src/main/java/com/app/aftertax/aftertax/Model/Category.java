package com.app.aftertax.aftertax.Model;

/**
 * Created by suhailbhat on 29/08/15.
 */
public class Category {
    public String name;
    public int id;
    public int taxTypeID;
    public int indexToPut;
    public int[] taxesIDs;
}
