package com.app.aftertax.aftertax.Model;

/**
 * Created by suhailbhat on 29/08/15.
 */
public class State {
    public String name;
    public int id;
    public String country;
    public MainTax[] taxes;
}
