package com.app.aftertax.aftertax.Model;

/**
 * Created by suhailbhat on 29/08/15.
 *
 * @property (nonatomic, strong) NSString *categoryName;
 * @property (nonatomic, assign) NSNumber *categoryID;
 * @property (nonatomic, strong) NSMutableArray *taxesApplicable;
 * @property (nonatomic, strong) NSNumber *indexToPut;
 * @property (nonatomic, strong) NSString *categoryDescription;
 */
public class Tax {
    public String name;
    public int id;
    public String unit;
    public float rate;
    public int limit;
    public String description;
    public float percent;
    public boolean GeneralTax;
}
