package com.app.aftertax.aftertax.Model;

/**
 * Created by suhailbhat on 29/08/15.
 */
public class JSONSource {
    public String version;
    public String updatedate;
    public String comment;
    public Category[] categories;
    public Country[] countries;
    public State[] states;
}
