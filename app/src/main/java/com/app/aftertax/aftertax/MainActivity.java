package com.app.aftertax.aftertax;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.aftertax.aftertax.Singleton.BigBoss;
import com.app.aftertax.aftertax.Singleton.TaxCalculator;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;


@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    public String str = "";
    Character op = 'q';
    float i, num, numtemp;
    TextView showResult, Value;
    // Declare Variables
    ListView listview;

    /*Parse objects*/
    List<ParseObject> ob;
    ArrayAdapter<String> adapter;
    private TextView tv1;

    // String[] spinnerSubs = { "Restaurant Dinner", "Groceries", "Clothes","Consumer Products", "Cigarettes", "Liquor","Beer", "Wine", "Champagne","Electronics", "Jewellery", "Prescribed Drugs","Non Prescribed Drugs"};
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;


    private Spinner Spin1;
    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);
            MainActivity.this.setupActionBarWithElements(BigBoss.getInstance().categoriesToShow);
        }
    };
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BigBoss.getInstance().dInitialize(MainActivity.this);
        // Execute RemoteDataTask AsyncTask
        /************************************/
        Firebase.setAndroidContext(this);
        Firebase mRef = new Firebase("https://aftertax-3aafc.firebaseio.com/");
        // DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        // DatabaseReference mRef = mRootRef.child("TaxJsonForAndroid");
        /***********************************/
        //new RemoteDataTask().execute();'''
        // Determine whether the current user is an anonymous user
        setContentView(R.layout.activity_main);

        /******************** testing taxjson ***********/
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("File Read Error: " + firebaseError.getMessage());
            }
        });
        /************************************************/

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        this.setupButtons();
        startService(new Intent(MainActivity.this, BigBoss.class));
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        // this.setupActionBarWithElements(spinnerSubs);
    }

    public void setupButtons() {
        ArrayList<TextView> buttons = new ArrayList<TextView>();
        TextView val1 = (TextView) this.findViewById(R.id.Text1);
        buttons.add(val1);
        TextView val2 = (TextView) this.findViewById(R.id.Text2);
        buttons.add(val2);
        TextView val3 = (TextView) this.findViewById(R.id.Text3);
        buttons.add(val3);
        TextView val4 = (TextView) this.findViewById(R.id.Text4);
        buttons.add(val4);
        TextView val5 = (TextView) this.findViewById(R.id.Text5);
        buttons.add(val5);
        TextView val6 = (TextView) this.findViewById(R.id.Text6);
        buttons.add(val6);
        TextView val7 = (TextView) this.findViewById(R.id.Text7);
        buttons.add(val7);
        TextView val8 = (TextView) this.findViewById(R.id.Text8);
        buttons.add(val8);
        TextView val9 = (TextView) this.findViewById(R.id.Text9);
        buttons.add(val9);
        TextView valDot = (TextView) this.findViewById(R.id.Textdecimal);
        buttons.add(valDot);
        TextView valZero = (TextView) this.findViewById(R.id.Textzero);
        buttons.add(valZero);
        TextView valClear = (TextView) this.findViewById(R.id.Textclear);
        buttons.add(valClear);
        TextView valMin = (TextView) this.findViewById(R.id.TextMin);
        buttons.add(valMin);
        TextView valTip = (TextView) this.findViewById(R.id.Texttip);
        buttons.add(valTip);
        TextView valDetails = (TextView) this.findViewById(R.id.Textdetails);
        buttons.add(valDetails);
        Typeface tp = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        for (TextView tv : buttons) {
            tv.setTypeface(tp);
        }
    }

    public void setupActionBarWithElements(ArrayList<String> strings) {
        if (BigBoss.getInstance() != null && BigBoss.getInstance().categoriesToShow != null) {
            strings = BigBoss.getInstance().categoriesToShow;
        }
        CatListAdapter adp = new CatListAdapter(this, BigBoss.getInstance().categoriesToShow);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.list);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        Spin1 = (Spinner) findViewById(R.id.spinner1);
        Spin1.setAdapter(adp);
        Spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String msupplier = Spin1.getSelectedItem().toString();
                BigBoss.getInstance().setCurrentCategorySelected(msupplier);
                Log.e("Selected item : ", msupplier);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    public void buttonPressed(View v) {
        // Let is analyse

        // Get value from TextView and convert to float value
        final TextView val1 = (TextView) this.findViewById(R.id.value);
        TextView enteredText = (TextView) v;
        val1.setText(val1.getText() + "" + enteredText.getText().toString());


        if (val1.getText().length() < 1) return;
        // Conversion

        float floatBeforeTax = Float.parseFloat(val1.getText().toString());   // You were missing this I guess
        //Log.d("Before Tax", floatBeforeTax);

        // Pass the float value to TaxCalculator and get the AfterTax
        String afterTax = String.valueOf(TaxCalculator.getInstance().getAfterTaxForAmountAndCategory(floatBeforeTax));

        // Update the result TextView with new float value after conversion to String
        TextView resultView = (TextView) findViewById(R.id.result);
        resultView.setText(afterTax);
        //resultView.setText(String.valueOf(floatBeforeTax));

    }

//    public void goToHome(View v) {
//        final TextView home = (TextView)this.findViewById(R.id.promotion);
//        Intent home = new Intent(this, Promotion.class);
//        startActivity(promo);
//    }

    public void clearTapped(View v) {
        final TextView val1 = (TextView) this.findViewById(R.id.value);
        TextView result = (TextView) this.findViewById(R.id.result);
        if (val1.getText().length() > 0) {
            val1.setText(val1.getText().subSequence(0, val1.getText().length() - 1));
        }
        if (result.getText().length() > 0) {
            result.setText(result.getText().subSequence(0, result.getText().length() - 1));
        }

        if (val1.getText().length() == 0) {
            result.setText("");
        }

        TextView clr = (TextView) findViewById(R.id.Textclear);
        clr.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                Toast.makeText(MainActivity.this, "Long press", Toast.LENGTH_LONG).show();


                return false;
            }
        });
    }

    public void goToPromo(View v) {
        final TextView setting = (TextView) this.findViewById(R.id.promotion);
        Intent promo = new Intent(this, Promotion.class);
        startActivity(promo);
    }

    public void goToNotification(View v) {
        final TextView setting = (TextView) this.findViewById(R.id.notification);
        Intent notif = new Intent(this, Notification.class);
        startActivity(notif);
    }

    public void goToSetting(View v) {
        final TextView setting = (TextView) this.findViewById(R.id.setting);
        Intent settingsIntent = new Intent(this, Settings.class);
        startActivity(settingsIntent);
    }

    public void goToLogin(View v) {
        final TextView setting = (TextView) this.findViewById(R.id.login);
        Intent login = new Intent(this, ATLoginSignupActivity.class);
        startActivity(login);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("categories_recieved"));
        super.onResume();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}
