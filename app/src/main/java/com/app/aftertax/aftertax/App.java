package com.app.aftertax.aftertax;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;


/**
 * Created by suhailbhat on 27/08/15.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "OJLyRYZBYnymTON2Sgh4XkOHQmWYCwsTVJCd3qQs", "vl7a7HWmc51JiZELrFxbEBCslpTyqW9iwVa6q4V1");
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove,this line.
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

    }
}
